# import vars
import math
from ..algorithm import vars

wayPoints = []

def angle_to_metres(lon, lat):
    t = []
    # print("###", lon * 1000)
    t.append(lon * 111321 * math.cos(lat * math.pi / 180))
    t.append(lat * 111134)  # metres
    return t

def metres_to_angle(x, y):
    t = []
    lat = y / 111134
    lon = x / 111321 / math.cos(lat * math.pi / 180)
    t.append(lon)
    t.append(lat)
    return t

def dist(x1, y1, z1, x2, y2, z2):
    a = (x1 - x2) * (x1 - x2)
    b = (y1 - y2) * (y1 - y2)
    c = (z1 - z2) * (z1 - z2)
    return math.sqrt(a + b + c)


def min_battery_to_shot_area(h, A, B):
    a1 = 2 * math.tan(A / 2 / math.pi * 180) * h
    b1 = 2 * math.tan(B / 2 / math.pi * 180) * h
    # print("###", h, a1, b1)
    n_a = abs(vars.target[0]['x'] - vars.target[2]['x']) / a1 + min(abs(vars.target[0]['x'] - vars.target[2]['x']) % a1, 1)
    n_b = abs(vars.target[0]['y'] - vars.target[2]['y']) / b1 + min(abs(vars.target[0]['y'] - vars.target[2]['y']) % b1, 1)
    wayPoints.clear()
    for i in range(0, int(n_a)):
        for j in range(0, int(n_b)):
            point_x = vars.target[0]['x'] - i * a1 - a1 / 2
            point_y = vars.target[0]['y'] + j * b1 + b1 / 2
            wayPoints.append([point_x, point_y])
    num_of_shots = n_a * n_b
    var1 = n_a * (abs(vars.target[0]['y'] - vars.target[2]['y']) - b1) + abs(vars.target[0]['x'] - vars.target[2]['x']) - a1
    var2 = abs(vars.target[0]['y'] - vars.target[2]['y']) - b1 + n_b * (abs(vars.target[0]['x'] - vars.target[2]['x']) - a1)
    return min(var1, var2) * vars.battery_usage_per_meter + num_of_shots * vars.battery_usage_shot





def start():
    for i in range(0, 4):
        # vars.target[i]['lon'] = float(vars.target[i]['lon'])
        # vars.target[i]['lat'] = float(vars.target[i]['lat'])
        t = angle_to_metres(vars.target[i]['lon'], vars.target[i]['lat'])
        vars.target[i]['x'] = t[0]
        vars.target[i]['y'] = t[1]
    # vars.drone['lon'] = 
    t = angle_to_metres(vars.drone['lon'], vars.drone['lat'])
    vars.drone['x'] = t[0]
    vars.drone['y'] = t[1]



def get_height():
    l = 1
    r = 1000
    var = 0
    while (r - l > 1):
        m = (l + r) / 2
        var1 = min_battery_to_shot_area(m, vars.A, vars.B)
        var2 = min_battery_to_shot_area(m, vars.B, vars.A)
        if (vars.battery_remaining -  var1 >= vars.battery_remaining / 100 * 5 or vars.battery_remaining -  var2 >= vars.battery_remaining / 100 * 5):
            r = m
        else:
            l = m
    var1 = min_battery_to_shot_area(l, vars.A, vars.B)
    var2 = min_battery_to_shot_area(l, vars.B, vars.A)
    if (vars.battery_remaining - var1 >= vars.battery_remaining / 100 * 5 or vars.battery_remaining - var2 >= vars.battery_remaining / 100 * 5):
        return l
    else:
        return r


def get_route():
    print(metres_to_angle(4533553.576875514, 3921489.340022245))
    wayPoints.clear()
    start()
    print(vars.target)
    final_h = get_height()
    min_battery_to_shot_area(final_h, vars.A, vars.B)
    print("###", final_h, wayPoints)
    for i in range(0, len(wayPoints)):
        t = metres_to_angle(wayPoints[i][0], wayPoints[i][1])
        wayPoints[i][0] = t[0]
        wayPoints[i][1] = t[1]


    # wayPoints.clear()
    # wayPoints.append([vars.target[0]['lon'], vars.target[0]['lat']])
    # wayPoints.append([vars.target[1]['lon'], vars.target[1]['lat']])
    # wayPoints.append([vars.target[2]['lon'], vars.target[2]['lat']])
    # wayPoints.append([vars.target[3]['lon'], vars.target[3]['lat']])
    return wayPoints
