import math

def angle_to_meters(lon, lat):
    t = []
    # print("###", lon * 1000)
    t.append(lon * 111321 * math.cos(lat * math.pi / 180))
    t.append(lat * 111134)  # metres
    return t

def meters_to_angle(x, y):
    t = []
    lat = y / 111134
    lon = x / 111321 / math.cos(lat * math.pi / 180)
    t.append(lon)
    t.append(lat)
    return t

def createDots(x1, y1, x3, y3, w, h):
    x2 = x3
    y2 = y1
    x4 = x1
    y4 = y1

    mas = []
    mx = w / 2
    my = h / 2
    H = abs(y1 - y3)
    W = abs(x1 - x3)
    isTurn = False

    while my < H - h:
        mx = w / 2
        while mx < W:
            mas.append((mx + x1, my + y1))
            mx+= w

        mx = W - w / 2
        mas.append((mx + x1, my + y1))
        my+= h
        mx+= w
        isTurn = not isTurn # to 1

        if my >= H:
            break
        mx-= w
        while mx > w / 2:
            mas.append((mx + x1, my + y1))
            mx-= w
        mx = w / 2
        mas.append((mx + x1, my + y1))
        my+= h
        isTurn = not isTurn # to 0

    my = H - h / 2
    if isTurn == False:
        mx = w / 2
        while mx < W:
            mas.append((mx + x1, my + y1))
            mx+= w

        mx = W - w / 2
        mas.append((mx + x1, my + y1))
    elif isTurn == True:
        mx = W - w / 2
        while mx > w / 2:
            mas.append((mx + x1, my + y1))
            mx-= w
        mx = w / 2
        mas.append((mx + x1, my + y1))
        my+= h



    return mas

# mas = createDots(0, 0, 50, 70, 20, 15)
# print(mas)

def dist(mas):
    res = 0
    for i in range(0, len(mas) - 1):
        # print("######", ((mas[i][0] * mas[i][0] - mas[i + 1][0] * mas[i + 1][0]) + (mas[i][1] * mas[i][1] - mas[i + 1][1] * mas[i + 1][1])))
        res+= math.sqrt((mas[i][0] - mas[i + 1][0]) * (mas[i][0] - mas[i + 1][0]) + (mas[i][1] - mas[i + 1][1]) * (mas[i][1] - mas[i + 1][1]))
    return res


def chooseHeight(x1, y1, x3, y3, d1, d2, bat, rash, alfa1, alfa2):
	l = 0
	r = 1000
	eps = 0.001

	mas = None
	x1, y1 = angle_to_meters(x1, y1)
	x3, y3 = angle_to_meters(x3, y3)
	d1, d2 = angle_to_meters(d1, d2)
	while r - l > eps:
		m = (r + l) / 2
		w = 2 * m * math.tan((alfa1 / 2) / 180 * math.pi)
		h = 2 * m * math.tan((alfa2 / 2) / 180 * math.pi)
		mas = createDots(x1, y1, x3, y3, w, h)
		mas.insert(0, (d1, d2))
		print(l, r, dist(mas) * rash, bat)
		if dist(mas) * rash > bat:
			l = m
		else:
			r = m
	print(mas)
	for i in range(0, len(mas)):
		# mas[i] = meters_to_angle(mas[i][0] + x1, mas[i][1] + y1)
		# continue
		if i > 0:
			mas[i] = meters_to_angle(mas[i][0] + w / 2 - x1 + x3, mas[i][1] - h / 2)
		else:
			mas[i] = meters_to_angle(mas[i][0], mas[i][1])

	return mas
